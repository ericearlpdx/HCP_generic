#!/bin/bash
#
# Combines settings for your protocol with the system's SetUpHCPPipeline.sh EnvironmentScript.  This script must be executable
#

export QUEUE="localhost" #Available queuing system options are "SGE" or "HTCondor" or "localhost"

### HCP PIPELINE DIRECTORY AND SETUP (DO NOT CHANGE THE ORDER OF THESE TWO LINES) ### replace below examples as necessary
export HCPPIPEDIR="/home/exacloud/lustre1/fnl_lab/code/HCP_release_20151207" #location of the github-cloned repository of the HCP pipeline
. /home/exacloud/lustre1/fnl_lab/code/SetUpHCPPipeline_Exacloud.sh # the ". " in front is very important to source the SetUp file for parent scripts.  Do not remove those two characters from the front.

### ATLAS SETTINGS ###
export T1Atlas="" # T1 template
export T1AtlasBrain="" #Brain extracted T1 template
export T1AtlasResamp="" #Resampled T1 template
export T2Atlas="" #T2 template
export T2AtlasBrain="" #Brain extracted T2 template
export T2AtlasResamp="" #Resampled T2 Template
export AtlasMask="" #T1/T2 template brain mask
export AtlasResampMask="" #Resampled T1/T2 template brain mask

### PROTOCOL SETTINGS ###
export CorrectionMethod="FIELDMAP" #Averaging and readout distortion correction methods: "NONE" = average any repeats with no readout correction "FIELDMAP" = average any repeats and use field map for readout correction "TOPUP" = average and distortion correct at the same time with topup/applytopup only works for 2 images currently
export FieldmapDeltaTE="2.46" #delta TE in ms for field map, OMD: 2.46 is good for our data. looking at the field map, echotimes are 7.65 and 5.19, so 7.65-5.19=2.46, this is the SIEMENS standard
export TopUpConfigFile="NONE" #Topup config if using TOPUP, set to NONE if using regular FIELDMAP

# Echo Spacing or Dwelltime of spin echo EPI MRI image. Specified in seconds.
# Set to "NONE" if not used. 
# 
# Dwelltime = 1/(BandwidthPerPixelPhaseEncode * # of phase encoding samples)
# DICOM field (0019,1028) = BandwidthPerPixelPhaseEncode
# DICOM field (0051,100b) = AcquisitionMatrixText first value (# of phase encoding samples).
# On Siemens, iPAT/GRAPPA factors have already been accounted for.  
#
# Example value for when using Spin Echo Field Maps:
#   0.000580002668012
export TopupDwellTime="NONE"

export TopupUnwarpDir="NONE" #x (left to right) or y (anterior to posterior) (minus or not does not matter) "NONE" if not used 
export T1Spacing="" #DICOM field (0019,1018) in seconds or "NONE" if not used
export T2Spacing="" #DICOM field (0019,1018) in seconds or "NONE" if not used
export FOVSize="150" #BrainSize in mm, 150 for humans, OMD: z direction (top), used to remove neck
export OutputFMRIResolution="2.0" #Target final resolution of fMRI data. 2mm is recommended for 3T HCP data, 1.6mm for 7T HCP data (i.e. should match acquired resolution).  Use 2.0 or 1.0 to avoid standard FSL templates
export GrayordinatesRes="2" #Usually 2mm, if multiple delimit with @, must already exist in templates dir
export HighResMeshKvertices="164" #Usually 164k vertices
export LowResMeshesKvertices="32" #Usually 32k vertices, if multiple delimit with @, must already exist in templates dir
export SmoothingSize="2.0" #Recommended to be roughly the voxel size (EPIResolution)

