#! /bin/bash
#
# Originally written by Eric Earl, OHSU, 1/11/2016
# Edited by Anders Perrone. OHSU, 09/29/2016 to include the "No T2" option

SETTINGS=$1 # path to your protocol settings shell file based on the template
SUBJECT=$2 # subject ID (used for folder name and NIFTI filenames)
PIPEDIR=$3 # path to the STUDY/SUBJECT/VISIT/PIPELINE folder

### RUN THE SETTINGS SCRIPT ###
if [ -e ${SETTINGS} ] ; then
  . ${SETTINGS}
else
  echo ${SETTINGS}' does not exist!  Quitting.'
  exit 1
fi

# default to not Macaque or Infant protocols.
NewInfantProtocol=${NewInfantProtocol:-false}
MacaqueProtocol=${MacaqueProtocol:-false}

VISITDIR=`dirname ${PIPEDIR}`
PROCDIR="${PIPEDIR}/${SUBJECT}"
SCRIPTDIR=${PROCDIR}/Scripts


### COPY OVER THE ORIGINAL/UNEDITED BATCH FILE EXAMPLES FROM THE REPOSITORY ###
### ALSO NEST THE SUBJECT UNDER THE VISIT DIRECTORY ###
if [ ! -d ${SCRIPTDIR} ]; then
    mkdir -p ${SCRIPTDIR}
fi

# Setup destination paths & script names
HCP_PRE=${SCRIPTDIR}/PreFreeSurferPipelineBatch.sh
HCP_FREE=${SCRIPTDIR}/FreeSurferPipelineBatch.sh
HCP_POST=${SCRIPTDIR}/PostFreeSurferPipelineBatch.sh
HCP_VOL=${SCRIPTDIR}/GenericfMRIVolumeProcessingPipelineBatch.sh
HCP_SURF=${SCRIPTDIR}/GenericfMRISurfaceProcessingPipelineBatch.sh
HCP_RUNNER=${SCRIPTDIR}/hcp_runner.sh
SETTINGS_COPY=${SCRIPTDIR}/ProtocolSettings_copy.sh

# Copy from HCP_release_blah/Examples/Scripts to populate the /Scripts destinations above
# SB: we may want to adjust this, or add a step / document that we must always update /Examples after new releases

cp ${HCPPIPEDIR}/Examples/Scripts/PreFreeSurferPipelineBatch.sh ${HCP_PRE}
cp ${HCPPIPEDIR}/Examples/Scripts/FreeSurferPipelineBatch.sh ${HCP_FREE}
cp ${HCPPIPEDIR}/Examples/Scripts/PostFreeSurferPipelineBatch.sh ${HCP_POST}
cp ${HCPPIPEDIR}/Examples/Scripts/GenericfMRIVolumeProcessingPipelineBatch.sh ${HCP_VOL}
cp ${HCPPIPEDIR}/Examples/Scripts/GenericfMRISurfaceProcessingPipelineBatch.sh ${HCP_SURF}
cp ${SETTINGS} ${SETTINGS_COPY}

# additional/replacement stages for divergent pipelines
if $NewInfantProtocol; then
  # additional Prep stage
  HCP_PREP=${SCRIPTDIR}/FNLPrepPipelineBatch.sh
  cp ${HCPPIPEDIR}/Examples/Scripts/FNLPrepPipelineBatch.sh ${HCP_PREP}
elif $MacaqueProtocol; then
  # additional Prep, fieldmap stages and redesigned Free
  HCP_PREP=${SCRIPTDIR}/FNLPrepPipelineBatch.sh
  HCP_FM=${SCRIPTDIR}/FMPipelineBatch.sh
  rm ${HCP_FREE}
  cp ${HCPPIPEDIR}/Examples/Scripts/FNLPrepPipelineBatch.sh ${HCP_PREP}
  cp ${HCPPIPEDIR}/Examples/Scripts/FMPipelineBatch.sh ${HCP_FM}
  cp ${HCPPIPEDIR}/Examples/Scripts/FreeGreyPipelineBatch.sh ${HCP_FREE}
fi

# To prevent permissions issues
chown -R `whoami`:fnl_lab ${VISITDIR}
chmod -R 770 ${VISITDIR}

### IF UNPROCESSED IS IN THE WRONG PLACE, MOVE UNPROCESSED INTO DIRECTORY TO PROCDIR ###
if [ -d ${PIPEDIR}/unprocessed ]
	then
	mv ${PIPEDIR}/unprocessed ${PROCDIR}/
fi

# Use sed to substitute /Example paths into the ones WE need per our local naming schemes
### HCP_PREP ###
if [ ! -z $HCP_PREP ]; then
  if [ $NewInfantProtocol ]; then
    sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_PREP
    sed -i 's|"${T1wInputImages}${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR${i}/${Subject}_3T_T1w_MPR${i}.nii.gz@"|"${T1wInputImages}${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_T1w_MPR${i}.nii.gz@"|g' $HCP_PRE
    sed -i 's|"${T2wInputImages}${StudyFolder}/${Subject}/unprocessed/3T/T2w_SPC${i}/${Subject}_3T_T2w_SPC${i}.nii.gz@"|"${T2wInputImages}${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_T2w_SPC${i}.nii.gz@"|g' $HCP_PRE
    sed -i 's|AvgrdcSTRING=".*"|AvgrdcSTRING="${CorrectionMethod}"|g' $HCP_PRE
    sed -i "s|StudyAtlasHead=\".*\"|StudyAtlasHead=\"${StudyAtlasHead}\"|g" $HCP_PREP
    sed -i "s|StudyAtlasBrain=\".*\"|StudyAtlasBrain=\"${StudyAtlasBrain}\"|g" $HCP_PREP
    sed -i "s|StudyAtlasAseg=\".*\"|StudyAtlasAseg=\"${StudyAtlasAseg}\"|g" $HCP_PREP
    sed -i "s|useT2=\".*\"|useT2=\"${useT2}\"|g" $HCP_PREP
    if [ ${CorrectionMethod} == "TOPUP" ] ; then
      sed -i 's|TopupConfig=".*"|TopupConfig="${TopUpConfigFile}"|g' $HCP_PREP
      sed -i 's|DwellTime=".*"|DwellTime="${TopupDwellTime}"|g' $HCP_PREP
      sed -i 's|SEUnwarpDir=".*"|SEUnwarpDir="${TopupUnwarpDir}"|g' $HCP_PREP
      sed -i 's|SpinEchoPhaseEncodeNegative="NONE"|SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodeNegative.nii.gz"|g' $HCP_PREP
      sed -i 's|SpinEchoPhaseEncodePositive="NONE"|SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodePositive.nii.gz"|g' $HCP_PREP
      sed -i 's|UseJacobian=".*"|UseJacobian="${UseJacobian}"|g' $HCP_PREP
    elif [ ${CorrectionMethod} == "NONE" ]; then
      sed -i 's|TopupConfig=".*"|TopupConfig="NONE"|g' $HCP_PREP
      sed -i 's|DwellTime=".*"|DwellTime="NONE"|g' $HCP_PREP
      sed -i 's|SEUnwarpDir=".*"|SEUnwarpDir="NONE"|g' $HCP_PREP
    fi
  elif [ $MacaqueProtocol ]; then
    break
  fi
fi
### HCP_PRE ###

# sed -i 's|||g' $HCP_PRE
sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_PRE

sed -i 's|"${T1wInputImages}${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR${i}/${Subject}_3T_T1w_MPR${i}.nii.gz@"|"${T1wInputImages}${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_T1w_MPR${i}.nii.gz@"|g' $HCP_PRE
sed -i 's|"${T2wInputImages}${StudyFolder}/${Subject}/unprocessed/3T/T2w_SPC${i}/${Subject}_3T_T2w_SPC${i}.nii.gz@"|"${T2wInputImages}${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_T2w_SPC${i}.nii.gz@"|g' $HCP_PRE

sed -i 's|T1wTemplate=".*"|T1wTemplate="${T1Atlas}"|g' $HCP_PRE
sed -i 's|T1wTemplateBrain=".*"|T1wTemplateBrain="${T1AtlasBrain}"|g' $HCP_PRE
sed -i 's|T1wTemplate2mm=".*"|T1wTemplate2mm="${T1AtlasResamp}"|g' $HCP_PRE
sed -i 's|T2wTemplate=".*"|T2wTemplate="${T2Atlas}"|g' $HCP_PRE
sed -i 's|T2wTemplateBrain=".*"|T2wTemplateBrain="${T2AtlasBrain}"|g' $HCP_PRE
sed -i 's|T2wTemplate2mm=".*"|T2wTemplate2mm="${T2AtlasResamp}"|g' $HCP_PRE
sed -i 's|TemplateMask=".*"|TemplateMask="${AtlasMask}"|g' $HCP_PRE
sed -i 's|Template2mmMask=".*"|Template2mmMask="${AtlasResampMask}"|g' $HCP_PRE

sed -i 's|AvgrdcSTRING=".*"|AvgrdcSTRING="${CorrectionMethod}"|g' $HCP_PRE
sed -i 's|T1wSampleSpacing=".*".*#|T1wSampleSpacing="${T1Spacing}" #|g' $HCP_PRE
sed -i 's|T2wSampleSpacing=".*".*#|T2wSampleSpacing="${T2Spacing}" #|g' $HCP_PRE
sed -i 's|BrainSize=".*"|BrainSize="${FOVSize}"|g' $HCP_PRE
sed -i 's|useT2=".*"|useT2="${useT2}"|g' $HCP_PRE

if [ ${CorrectionMethod} == "TOPUP" ] ; then
  sed -i 's|TopupConfig=".*"|TopupConfig="${TopUpConfigFile}"|g' $HCP_PRE
  sed -i 's|DwellTime=".*"|DwellTime="${TopupDwellTime}"|g' $HCP_PRE
  sed -i 's|SEUnwarpDir=".*"|SEUnwarpDir="${TopupUnwarpDir}"|g' $HCP_PRE
  sed -i 's|SpinEchoPhaseEncodeNegative="NONE"|SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodeNegative.nii.gz"|g' $HCP_PRE
  sed -i 's|SpinEchoPhaseEncodePositive="NONE"|SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodePositive.nii.gz"|g' $HCP_PRE
  sed -i 's|UseJacobian=".*"|UseJacobian="${UseJacobian}"|g' $HCP_PRE
else
  sed -i 's|TopupConfig=".*"|TopupConfig="NONE"|g' $HCP_PRE
  sed -i 's|DwellTime=".*"|DwellTime="NONE"|g' $HCP_PRE
  sed -i 's|SEUnwarpDir=".*"|SEUnwarpDir="NONE"|g' $HCP_PRE
fi

if [ ${CorrectionMethod} == "FIELDMAP" ] ; then
  sed -i 's|DwellTime=".*"|DwellTime="${TopupDwellTime}"|g' $HCP_PRE
  sed -i 's|TE=".*"|TE="${FieldmapDeltaTE}"|g' $HCP_PRE
  sed -i 's|${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR1/${Subject}_3T_FieldMap_Magnitude.nii.gz|"${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Magnitude.nii.gz"|g' $HCP_PRE
  sed -i 's|${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR1/${Subject}_3T_FieldMap_Phase.nii.gz|"${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Phase.nii.gz"|g' $HCP_PRE
  sed -i 's|MagnitudeInputName=".*"|MagnitudeInputName="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Magnitude.nii.gz"|g' $HCP_PRE
  sed -i 's|PhaseInputName=".*"|PhaseInputName="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Phase.nii.gz"|g' $HCP_PRE
else
  sed -i 's|TE=".*"|TE="NONE"|g' $HCP_PRE
  sed -i 's|"${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR1/${Subject}_3T_FieldMap_Magnitude.nii.gz"|"NONE"|g' $HCP_PRE
  sed -i 's|"${StudyFolder}/${Subject}/unprocessed/3T/T1w_MPR1/${Subject}_3T_FieldMap_Phase.nii.gz"|"NONE"|g' $HCP_PRE
fi

sed -i 's|/unprocessed/3T|/unprocessed/NIFTI|g' $HCP_PRE

### HCP_FREE ###

# sed -i 's|||g' $HCP_FREE
sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_FREE
sed -i 's|useT2=".*"|useT2="${useT2}"|g' $HCP_FREE

### HCP_POST ###

# sed -i 's|||g' $HCP_POST
sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_POST

sed -i 's|GrayordinatesResolutions=".*"|GrayordinatesResolutions="${GrayordinatesRes}"|g' $HCP_POST
sed -i 's|HighResMesh=".*"|HighResMesh="${HighResMeshKvertices}"|g' $HCP_POST
sed -i 's|LowResMeshes=".*"|LowResMeshes="${LowResMeshesKvertices}"|g' $HCP_POST
sed -i 's|useT2=".*"|useT2="${useT2}"|g' $HCP_POST

### HCP_FM ###
if [ ! -z $HCP_FM ]; then
  break
fi

### HCP_VOL ###

# sed -i 's|||g' $HCP_VOL
sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_VOL

sed -i 's|fMRITimeSeries="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_${fMRIName}.nii.gz"|fMRITimeSeries="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_${fMRIName}.nii.gz"|g' $HCP_VOL

if [ -e "${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_${fMRIName}_SBRef.nii.gz" ] ; then
    sed -i 's|fMRISBRef="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_${fMRIName}_SBRef.nii.gz"|fMRISBRef="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_${fMRIName}_SBRef.nii.gz"|g' $HCP_VOL
else
    sed -i 's|fMRISBRef="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_${fMRIName}_SBRef.nii.gz"|fMRISBRef="NONE"|g' $HCP_VOL
fi

sed -i 's|DistortionCorrection=".*"|DistortionCorrection="${CorrectionMethod}"|g' $HCP_VOL
sed -i 's|FinalFMRIResolution=".*"|FinalFMRIResolution="${OutputFMRIResolution}"|g' $HCP_VOL

# SB: flags added per HCP_release_20160412

sed -i 's|BiasCorrection=".*"|BiasCorrection="${BiasCorrection}"|g' $HCP_VOL
sed -i 's|MotionCorrectionType=".*"|MotionCorrectionType="${MotionCorrectionType}"|g' $HCP_VOL
sed -i 's|UseJacobian=".*"|UseJacobian="${UseJacobian}"|g' $HCP_VOL
sed -i 's|useT2=".*"|useT2="${useT2}"|g' $HCP_VOL


if [ ${CorrectionMethod} == "TOPUP" ] ; then
  sed -i 's|DwellTime=".*"|DwellTime="${TopupDwellTime}"|g' $HCP_VOL
  sed -i 's|SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_LR.nii.gz"|SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodeNegative.nii.gz"|g' $HCP_VOL
  sed -i 's|SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_RL.nii.gz"|SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_SpinEchoPhaseEncodePositive.nii.gz"|g' $HCP_VOL
  sed -i 's|TopUpConfig=".*"|TopUpConfig="${TopUpConfigFile}"|g' $HCP_VOL
else
  sed -i 's|DwellTime=".*"|DwellTime="NONE"|g' $HCP_VOL
  sed -i 's|SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_LR.nii.gz"|SpinEchoPhaseEncodeNegative="NONE"|g' $HCP_VOL
  sed -i 's|SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_RL.nii.gz"|SpinEchoPhaseEncodePositive="NONE"|g' $HCP_VOL
  sed -i 's|TopUpConfig=".*"|TopUpConfig="NONE"|g' $HCP_VOL
fi

if [ ${CorrectionMethod} == "FIELDMAP" ] ; then
  sed -i 's|DwellTime=".*"|DwellTime="${TopupDwellTime}"|g' $HCP_VOL
  sed -i 's|MagnitudeInputName=".*"|MagnitudeInputName="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Magnitude.nii.gz"|g' $HCP_VOL
  sed -i 's|PhaseInputName=".*"|PhaseInputName="${StudyFolder}/${Subject}/unprocessed/NIFTI/${Subject}_FieldMap_Phase.nii.gz"|g' $HCP_VOL
  sed -i 's|DeltaTE=".*"|DeltaTE="${FieldmapDeltaTE}"|g' $HCP_VOL
else
  sed -i 's|MagnitudeInputName=".*"|MagnitudeInputName="NONE"|g' $HCP_VOL
  sed -i 's|PhaseInputName=".*"|PhaseInputName="NONE"|g' $HCP_VOL
  sed -i 's|DeltaTE=".*"|DeltaTE="NONE"|g' $HCP_VOL
fi

commentLine=`grep -n '(space-delimited)' $HCP_VOL | awk '{ print $1 }' | sed 's|:.*||g'`
iLine=`grep -n 'i=1' $HCP_VOL | awk '{ print $1 }' | sed 's|:.*||g'`

head -n ${commentLine} ${HCP_VOL} > ${HCP_VOL}.temp
echo '' >> ${HCP_VOL}.temp
echo '# Start or launch pipeline processing for each subject' >> ${HCP_VOL}.temp
echo 'for Subject in $Subjlist ; do' >> ${HCP_VOL}.temp
echo '  echo $Subject' >> ${HCP_VOL}.temp
echo '  ' >> ${HCP_VOL}.temp
echo '  Tasklist=""' >> ${HCP_VOL}.temp
echo '  PhaseEncodinglist=""' >> ${HCP_VOL}.temp
echo '  ' >> ${HCP_VOL}.temp
echo '  #Look automatically for resting states' >> ${HCP_VOL}.temp
echo '  numRests=`ls ${StudyFolder}/${Subject}/unprocessed/NIFTI/*_REST*.nii.gz | wc -l`' >> ${HCP_VOL}.temp
echo '  i=1' >> ${HCP_VOL}.temp
echo '  while [ $i -le $numRests ] ; do' >> ${HCP_VOL}.temp
echo '    Tasklist="REST${i} ${Tasklist}"' >> ${HCP_VOL}.temp
echo '    PhaseEncodinglist="${TopupUnwarpDir} ${PhaseEncodinglist}"' >> ${HCP_VOL}.temp
echo '    i=$(($i+1))' >> ${HCP_VOL}.temp
echo '  done' >> ${HCP_VOL}.temp
echo '  ' >> ${HCP_VOL}.temp
tail -n +${iLine} ${HCP_VOL} >> ${HCP_VOL}.temp

mv -f ${HCP_VOL}.temp ${HCP_VOL}
chmod a+x ${HCP_VOL}

### HCP_SURF ###

# sed -i 's|||g' $HCP_SURF
sed -i "s|EnvironmentScript=\".*\"|EnvironmentScript=\"${SETTINGS_COPY}\"|g" $HCP_SURF

sed -i 's|LowResMesh=".*"|LowResMesh="${LowResMeshesKvertices}"|g' $HCP_SURF
sed -i 's|FinalfMRIResolution=".*"|FinalfMRIResolution="${OutputFMRIResolution}"|g' $HCP_SURF
sed -i 's|SmoothingFWHM=".*"|SmoothingFWHM="${SmoothingSize}"|g' $HCP_SURF
sed -i 's|GrayordinatesResolution=".*"|GrayordinatesResolution="${GrayordinatesRes}"|g' $HCP_SURF
sed -i 's|useT2=".*"|useT2="${useT2}"|g' $HCP_SURF


workLine=`grep -n 'DO WORK' $HCP_SURF | awk '{ print $1 }' | sed 's|:.*||g'`
forLoopLine=`grep -n 'for fMRIName in $Tasklist' $HCP_SURF | awk '{ print $1 }' | sed 's|:.*||g'`

head -n ${workLine} ${HCP_SURF} > ${HCP_SURF}.temp
echo '' >> ${HCP_SURF}.temp
echo 'for Subject in $Subjlist ; do' >> ${HCP_SURF}.temp
echo '  echo $Subject' >> ${HCP_SURF}.temp
echo '  ' >> ${HCP_SURF}.temp
echo '  Tasklist=""' >> ${HCP_SURF}.temp
echo '  ' >> ${HCP_SURF}.temp
echo '  #Look automatically for resting states' >> ${HCP_SURF}.temp
echo '  numRests=`ls ${StudyFolder}/${Subject}/unprocessed/NIFTI/*_REST*.nii.gz | wc -l`' >> ${HCP_SURF}.temp
echo '  i=1' >> ${HCP_SURF}.temp
echo '  while [ $i -le $numRests ] ; do' >> ${HCP_SURF}.temp
echo '    Tasklist="REST${i} ${Tasklist}"' >> ${HCP_SURF}.temp
echo '    i=$(($i+1))' >> ${HCP_SURF}.temp
echo '  done' >> ${HCP_SURF}.temp
echo '  ' >> ${HCP_SURF}.temp
tail -n +${forLoopLine} ${HCP_SURF} >> ${HCP_SURF}.temp

mv -f ${HCP_SURF}.temp ${HCP_SURF}
chmod a+x ${HCP_SURF}

### CALL THE SCRIPTS ###

if [ ${QUEUE} == "SGE" ] ; then
  if [ ! -z $HCP_PREP ]; then SUBMIT_PREP="qsub -sync y -l mf=4G,h_rt=4:: -N HCP_PREP -o ${SCRIPTDIR} -e ${SCRIPTDIR}"; fi
  SUBMIT_PRE="qsub -sync y -l mf=4G,h_rt=4:: -N HCP_PRE -o ${SCRIPTDIR} -e ${SCRIPTDIR}"
  SUBMIT_FREE="qsub -sync y -l mf=6G,h_rt=48:: -N HCP_FREE -o ${SCRIPTDIR} -e ${SCRIPTDIR}"
  SUBMIT_POST="qsub -sync y -l mf=8G,h_rt=4:: -N HCP_POST -o ${SCRIPTDIR} -e ${SCRIPTDIR}"
  if [ ! -z $HCP_FM ]; then SUBMIT_FM="qsub -sync y -l mf=4G,h_rt=4:: -N HCP_FM -o ${SCRIPTDIR} -e ${SCRIPTDIR}"; fi
  SUBMIT_VOL="qsub -sync y -l mf=8G,h_rt=48:: -N HCP_VOL -o ${SCRIPTDIR} -e ${SCRIPTDIR}"
  SUBMIT_SURF="qsub -sync y -l mf=8G,h_rt=12:: -N HCP_SURF -o ${SCRIPTDIR} -e ${SCRIPTDIR}"
fi

if [ ${QUEUE} == "HTCondor" ] ; then
  if [ ! -z $HCP_PREP ]; then SUBMIT_PREP="condor_submit"; fi
  SUBMIT_PRE="condor_submit"
  SUBMIT_FREE="condor_submit"
  SUBMIT_POST="condor_submit"
  if [ ! -z $HCP_FM ]; then SUBMIT_FM="condor_submit"; fi
  SUBMIT_VOL="condor_submit"
  SUBMIT_SURF="condor_submit"
fi

if [ ${QUEUE} == "localhost" ] ; then
  if [ ! -z $HCP_PREP ]; then SUBMIT_PREP=""; fi
  SUBMIT_PRE=""
  SUBMIT_FREE=""
  SUBMIT_POST=""
  if [ ! -z $HCP_FM ]; then SUBMIT_FM=""; fi
  SUBMIT_VOL=""
  SUBMIT_SURF=""
fi

echo '#! /bin/bash' > ${HCP_RUNNER}
echo '' >> ${HCP_RUNNER}
if [ ! -z $HCP_PREP ]; then echo ${SUBMIT_PREP}' '${HCP_PREP}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}; fi
echo ${SUBMIT_PRE}' '${HCP_PRE}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}
echo ${SUBMIT_FREE}' '${HCP_FREE}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}
echo ${SUBMIT_POST}' '${HCP_POST}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}
if [ ! -z $HCP_FM ]; then echo ${SUBMIT_FM}' '${HCP_FM}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}; fi
echo ${SUBMIT_VOL}' '${HCP_VOL}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}
echo ${SUBMIT_SURF}' '${HCP_SURF}' --runlocal --Subjlist="'${SUBJECT}'" --StudyFolder="'${PIPEDIR}'"' >> ${HCP_RUNNER}
chmod a+x ${HCP_RUNNER}

echo "hcp_wrapper.sh ran start to finish!  Use hcp_runner.sh (command below) in the Scripts directory to run all steps in serial.  Scripts directory here: ${SCRIPTDIR}"
echo "nohup ${HCP_RUNNER} > ${HCP_RUNNER}.log &"
